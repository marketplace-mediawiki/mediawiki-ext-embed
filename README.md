# Information / Информация

Добавление встроенного содержимого в статью.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Embed`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Embed' );
```

## Syntax / Синтаксис

```html
{{#embed: [URL]}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
